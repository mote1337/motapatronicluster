FROM ubuntu
# -- Start Initial
RUN apt update
RUN apt upgrade -y
RUN apt install python3 -y
RUN apt install python3-pip -y
RUN pip install psycopg[binary]
RUN pip install patroni[etcd]
# -- End Initial
RUN apt install net-tools -y
RUN apt install haproxy -y
COPY ./haproxyDIR/haproxy.cfg /etc/haproxy/haproxy.cfg
WORKDIR /etc/haproxy
ENTRYPOINT haproxy -f haproxy.cfg