FROM ubuntu
# -- Start Initial
RUN apt update
RUN apt upgrade -y
RUN apt install python3 -y
RUN apt install python3-pip -y
RUN pip install psycopg[binary]
RUN pip install patroni[etcd]
# -- End Initial
RUN apt install etcd -y
WORKDIR /workdir
COPY ./etcdDIR/etcd3.yaml /workdir/etcd.yaml
ENTRYPOINT etcd --config-file etcd.yaml