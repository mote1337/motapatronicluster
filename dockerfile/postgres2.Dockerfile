FROM ubuntu
# -- Start Initial
RUN apt update
RUN apt upgrade -y
RUN apt install python3 -y
RUN apt install python3-pip -y
RUN pip install psycopg[binary]
RUN pip install patroni[etcd]
# -- End Initial
RUN echo 'tzdata tzdata/Areas select Europe' | debconf-set-selections
RUN echo 'tzdata tzdata/Zones/Europe select Rome' | debconf-set-selections
RUN DEBIAN_FRONTEND="noninteractive" apt install -y tzdata
RUN apt install postgresql postgresql-contrib -y
RUN ln -s /usr/lib/postgresql/14/bin/* /usr/sbin/
RUN mkdir /var/lib/patroni
RUN chown postgres:postgres /var/lib/patroni
RUN chmod 750 /var/lib/patroni
USER postgres
WORKDIR /workdir
COPY ./postgresDIR/patroni2.yaml /workdir/patroni.yaml
ENTRYPOINT patroni patroni.yaml