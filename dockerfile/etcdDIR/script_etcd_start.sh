#!/bin/bash

echo $MC1
echo $MC2
echo $MC3
echo $NUMBER_CLUSTER
echo "--------"

etcd --name infra0 --initial-advertise-peer-urls http://$MC1:2380 \
  --listen-peer-urls http://$MC1:2380 \
  --listen-client-urls http://$MC1:2379,http://127.0.0.1:2379 \
  --advertise-client-urls http://$MC1:2379 \
  --initial-cluster-token etcd-cluster-$NUMBER_CLUSTER \
  --initial-cluster infra0=http://$MC1:2380,infra1=http://$MC2:2380,infra2=http://$MC3:2380 \
  --initial-cluster-state new